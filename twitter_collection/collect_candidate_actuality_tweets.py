import tweepy
import Twitter_collect.twitter_connection_setup
from tweepy import OAuthHandler

def get_candidate_queries(num_candidate, file_path, keyword_type):
    """
    Generate and return a list of string queries for the search Twitter API from the file file_path_num_candidate.txt
    :param num_candidate: the number of the candidate
    :param file_path: the path to the keyword and hashtag
    files
    :param type: type of the keyword, either "keywords" or "hashtags"
    :return: (list) a list of string queries that can be done to the search API independently
    """
    filename = "{0}_{1}".format(keyword_type, num_candidate)
    try:
       with open("file_path/" + filename + "/txt") as f:
       Queries_list = [str(line for line in f.readlines())]

       return Queries_list

    except IOError :
        print("File not found", "please make sure the correct path is inquired")


def get_tweets_from_candidates_search_queries(queries, twitter_api):

    try:
        tweets = twitter_api.search( str(query for query in queries),language="french",rpp=100)
        for tweet in tweets:
            print(tweet.text)

    except tweepy.RateLimitError :
        print('Twitter API rate limit error. Please try again in 15 minutes')
    except tweepy.TweepError():
        print('Make sure your API authentification tokens are correct')


#listener = StdOutListener()
#stream=tweepy.Stream(auth = twitter_api.auth, listener=listener)
#stream.filter(track=[str(query for query in queries)])



from tweepy.streaming import StreamListener
class StdOutListener(StreamListener):

    def on_data(self, data):
        print(data)
        return True

    def on_error(self, status):
        if  str(status) == "420":
            print(status)
            print("You exceed a limited number of attempts to connect to the streaming API")
            return False
        else:
            return True
# si l'erreur 420 se produit, on déconnecte notre stream

def collect_by_streaming_tweets_about_and_replies(num_candidate, file_path, keyword_type):

    connexion = Twitter_collect.twitter_connection_setup.twitter_setup()
    listener = StdOutListener()
    stream=tweepy.Stream(auth = connexion.auth, listener=listener)
    stream.filter(track=[str(query) for query in get_candidate_queries(num_candidate, file_path, keyword_type)])


