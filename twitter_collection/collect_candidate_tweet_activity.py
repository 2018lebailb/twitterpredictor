import tweepy
import sys
import Twitter_collect.twitter_connection_setup
from Twitter_collect.credentials import *

auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_SECRET)
api = tweepy.API(auth)

def get_replies_to_candidate(num_candidate):
    replies=[]
    non_bmp_map = dict.fromkeys(range(0x10000, sys.maxunicode + 1), 0xfffd)
    for full_tweets in api.user_timeline(user_id=num_candidate, count=5):
        for tweet in api.search(q = 'to:@'+ api.get_user(num_candidate).name, count=10):
            if hasattr(tweet,'in_reply_to_status_id_str'):
                if tweet.in_reply_to_status_id_str==full_tweets.id_str:
                    replies.append(tweet.text)
        print("Tweet :",full_tweets.text.translate(non_bmp_map))
        for elements in replies:
            print("Replies :",elements)
        replies.clear()

def get_retweets_of_candidate(num_candidate):
    retweets=[]
    non_bmp_map = dict.fromkeys(range(0x10000, sys.maxunicode + 1), 0xfffd)
    for full_tweets in api.user_timeline(user_id=num_candidate, count=1):
        for retweet in api.retweets(full_tweets.id) :
                    retweets.append(retweet.text)
        print("Tweet :",full_tweets.text.translate(non_bmp_map))
        for elements in retweets:
            print("Retweets :",elements)
        retweets.clear()
