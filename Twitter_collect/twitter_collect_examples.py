# API Search

import tweepy
import Twitter_collect.twitter_connection_setup


def collect():
    connexion = Twitter_collect.twitter_connection_setup.twitter_setup()        # on se connecte à l'API Twitter
    tweets = connexion.search("Emmanuel Macron",language="french",rpp=100)  # on récupère uniquement les tweets avec certaines caractéristiques
    for tweet in tweets:
        print(tweet)             # on print les tweets correspondants à nos critères de sélection



# Les différentes requêtes à tester
"Emmanuel Macron"
"Emmanuel and Macron"
"Emmanuel AND Macron"
"Emmanuel OR Macron"
"Emmanuel -Macron"
"#EmmanuelMacron"
"@EmmanuelMacron"

# API Users

def collect_by_user(user_id):
    connexion = Twitter_collect.twitter_connection_setup.twitter_setup()
    statuses = connexion.user_timeline(id = user_id, count = 200)
    for status in statuses:
        print(status.text)
    return statuses
# return les count=200 statuts les plus récents pour le user user_id

collect_by_user(797514990420889600)

# fonctionne correctement pour un id_user pris au hasard

# API Streaming

from tweepy.streaming import StreamListener
class StdOutListener(StreamListener):

    def on_data(self, data):
        print(data)
        return True

    def on_error(self, status):
        if  str(status) == "420":
            print(status)
            print("You exceed a limited number of attempts to connect to the streaming API")
            return False
        else:
            return True
# si l'erreur 420 se produit, on déconnecte notre stream

def collect_by_streaming():

    connexion = Twitter_collect.twitter_connection_setup.twitter_setup()
    listener = StdOutListener()
    stream=tweepy.Stream(auth = connexion.auth, listener=listener)
    stream.filter(track=['Emmanuel Macron'])
    # permet d’afficher en temps réel les tweets publiés en continu pour un mot clé choisi (ici 'Emmanuel Macron')

    stream.disconnect()
    del stream
    # on supprime le stream qu'on est en train de faire ou on fait ctrl + c dans la console python

# test des différentes fonctions :

def test_func():
    # on peut uniquement vérifier que les fonctions écrites ne renvoient pas des requêtes nulles : OK
