def get_candidate_queries(num_candidate, file_path, keyword_type):
    """
    Generate and return a list of string queries for the search Twitter API from the file file_path_num_candidate.txt
    :param num_candidate: the number of the candidate
    :param file_path: the path to the keyword and hashtag
    files
    :param type: type of the keyword, either "keywords" or "hashtags"
    :return: (list) a list of string queries that can be done to the search API independently
    """
    filename = str("{0}_{1}".format(keyword_type, str(num_candidate)))
    try:
       with open("file_path/" + filename + "/txt") as f:
       Queries_list = [str(line for line in f.readlines())]

       return Queries_list

    except IOError :
        print("File not found", "please make sure the correct path is inquired")

