import tweepy
from tweepy import OAuthHandler
import Twitter_collect.twitter_connection_setup
import twitter_collection.collect_candidate_actuality_tweets.py
import twitter_collection.collect_candidate_tweet_activity.py


def global_collect(num_candidate, file_path):
    Queries = twitter_collection.collect_candidate_actuality_tweets.py.get_candidate_queries(num_candidate, file_path, "hashtags") + twitter_collection.collect_candidate_actuality_tweets.py.get_candidate_queries(num_candidate, file_path, "keywords")
    twitter_collection.collect_candidate_actuality_tweets.py.get_tweets_from_candidates_search_queries(Queries, Twitter_collect.twitter_connection_setup.twitter_setup())

    twitter_collection.collect_candidate_tweet_activity.py.get_retweets_of_candidate(num_candidate)
    twitter_collection.collect_candidate_tweet_activity.py.get_replies_to_candidate(num_candidate)
