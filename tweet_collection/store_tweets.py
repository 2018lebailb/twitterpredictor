import tweepy
import Twitter_collect.twitter_connection_setup
import numpy

def collect_list():
    list=[]
    connexion = Twitter_collect.twitter_connection_setup.twitter_setup()
    tweets = connexion.search("Emmanuel Macron",language="french",rpp=100)
    for tweet in tweets:
        list.append(tweet)
    return list


a=collect_list()

import json
def store_tweets(tweets):
    filename = input(print('choisissez le nom de votre fichier'))
    tmp_list=[]
    with open( filename +".json",'w') as file :
        for tweet in tweets:
            tweet_as_dict={"text":tweet.text,"user":tweet.id,"date of the tweet":str(tweet.created_at),"reply":tweet.in_reply_to_status_id,"number of retweets":tweet.retweet_count,"likes":tweet.favorite_count}
            tmp_list.append(tweet_as_dict)
        json.dump(tmp_list,file)


# faire une liste de tweets à partir d'un ensemble de fichiers jason
import json

tweet_files = ['file_1.json', 'file_2.json', ...] # on lit nos fichiers jason puis on fait une liste nommée tweets
tweets = []
for file in tweet_files:
    with open(file, 'r') as f:
        for line in f.readlines():
            tweets.append(json.loads(line))


# stocker de l'information intéressante d'un ensemble de tweets
import numpy as np
import tweepy
import Twitter_collect.twitter_connection_setup

def collect_to_pandas_dataframe():
   connexion = Twitter_collect.twitter_connection_setup.twitter_setup()
   tweets = connexion.search("@EmmanuelMacron",language="fr",rpp=100)
   data = pd.DataFrame(data=[tweet.text for tweet in tweets], columns=['tweet_textual_content'])
   data['len']  = np.array([len(tweet.text) for tweet in tweets])
   data['ID']   = np.array([tweet.id for tweet in tweets])
   data['Date'] = np.array([tweet.created_at for tweet in tweets])
   data['Source'] = np.array([tweet.source for tweet in tweets])
   data['Likes']  = np.array([tweet.favorite_count for tweet in tweets])
   data['RTs']    = np.array([tweet.retweet_count for tweet in tweets])
   return data
