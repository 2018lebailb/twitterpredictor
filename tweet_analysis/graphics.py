import pandas as pd
import matplotlib.pyplot as plt
import tweet_analysis.tweet_analysis

data = tweet_analysis.tweet_analysis.collect_to_pandas_dataframe()
tfav = pd.Series(data=data['Likes'].values, index=data['Date'])
tret = pd.Series(data=data['RTs'].values, index=data['Date'])

# Likes vs retweets visualization:
tfav.plot(figsize=(16,4), label="Likes", legend=True)
tret.plot(figsize=(16,4), label="Retweets", legend=True)

plt.show()

# on retrouve bien la bonne figure : likes et retweets en fonction du temps
# il suffit de modifier les paramètres à afficher pour obtenir d'autres informations d'évolution
