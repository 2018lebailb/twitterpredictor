import numpy as np
import Twitter_collect.twitter_connection_setup
import pandas as pd

def collect_to_pandas_dataframe():
   connexion = Twitter_collect.twitter_connection_setup.twitter_setup()
   tweets = connexion.search("@EmmanuelMacron",language="fr",rpp=100)
   data = pd.DataFrame(data=[tweet.text for tweet in tweets], columns=['tweet_textual_content'])
   data['len']  = np.array([len(tweet.text) for tweet in tweets])
   data['ID']   = np.array([tweet.id for tweet in tweets])
   data['Date'] = np.array([tweet.created_at for tweet in tweets])
   data['Source'] = np.array([tweet.source for tweet in tweets])
   data['Likes']  = np.array([tweet.favorite_count for tweet in tweets])
   data['RTs']    = np.array([tweet.retweet_count for tweet in tweets])
   return data

#collect_to_pandas_dataframe()
#                                tweet_textual_content  len  ...   Likes   RTs
#0   RT @Elysee: Au Maroc, le Président @EmmanuelMa...  140  ...       0   342
#1   @MarleneSchiappa La #PMAsansPère est une #disc...  128  ...       0     0
#2   RT @AdelineNoirmain: La multiplication des tax...  140  ...       0    56
#3   ça, OUI !!! #17novembre2018 @EmmanuelMacron @F...  131  ...       0     0
#4   RT @DoussietClaude: Le perroquet parle! Ils on...  140  ...       0     6
#5   RT @SimmiAhuja_: What's difference between cop...  136  ...       0     6
#6   RT @sabrinalinterne: Les yeux de l’humiliation...  140  ...       0   548
#7   "La transition vers 50% de nucléaire, je vais ...  140  ...       0     0
#8   RT @angry_dog17: La véritable raison pour laqu...  140  ...       0    16
#9   @TwiTerakount @les_repliques @EmmanuelMacron @...  140  ...       0     0
#10  RT @FLOTUS: Thank you @EmmanuelMacron and Mrs....  140  ...       0  9003
#11  RT @a2rak: Pardon pour l’anglais. @POTUS @real...   97  ...       0     4
#12  Toujours aussi méprisants, à l’image de ce pré...  135  ...       0     0
#13  @EmmanuelMacron Sous couvert du dérèglement cl...  140  ...       0     0
#14  RT @sabrinalinterne: Les yeux de l’humiliation...  140  ...       0   548
