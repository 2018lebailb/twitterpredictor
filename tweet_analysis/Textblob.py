from textblob import Word

import tweepy
import Twitter_collect.twitter_connection_setup


def collect_list():
    tweets_list=[]
    connexion = Twitter_collect.twitter_connection_setup.twitter_setup()        # on se connecte à l'API Twitter
    tweets = connexion.search("Emmanuel Macron",language="french",rpp=100)  # on récupère uniquement les tweets avec certaines caractéristiques
    for tweet in tweets:
        tweets_list.append(tweet)
    return tweets_list

import textblob
from textblob import TextBlob
import nltk

def get_vocabulary_from_tweets(tweets):
    tweets_list=[]                           #liste avec la liste des mots des différents tweets
    for tweet in tweets:
        tweet_list=[]
        for word in TextBlob(tweet.text):                     # on lemmatize et récupère les mots uniques d'un tweet de tweets
            word = Word(word)
            word.lemmatize()
            if TextBlob(tweet.text).words.count(str(word)) == 1:
                tweet_list.append(word)
        tweets_list.append(set(tweet_list))           # on conserve uniquement les mots intéressants en un seul exemplaire
    return tweets_list
